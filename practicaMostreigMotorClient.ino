#include <ArduinoWebsockets.h>
#include <WiFi.h>

#define NUM_BUFFERS 2
#define NUM_SAMPLES_PACKET 1000

#define ANALOG_READ_PIN 34

const char* ssid = "MasterIIoT"; //Enter SSID
const char* password = "2454441629"; //Enter Password
const char* websockets_server = "ws://192.168.1.7:8080"; //server adress and port

using namespace websockets;

//MUTEX
portMUX_TYPE DRAM_ATTR timerMux = portMUX_INITIALIZER_UNLOCKED; 
//Tasca consumidor -> agafa N_SAMPLES_COUNT del buffer circular i els envia pel websocket
TaskHandle_t consumidorTask;
hw_timer_t * timerproducer = NULL; // our timer+

//tbStringCircularBuffer queue(NUM_SAMPLES_PACKET);
String buffers[NUM_BUFFERS];
byte current_buffer=0;
int num_elements=0;

WebsocketsClient client;


//NOTA:
//Productor: LLegeix una senyal analògica amb una freqüència de 12KHz i va posant els valors llegits a un buffer
//Consumidor: Quan el buffer supera un nombre determinat d'alements es desperta una tasac que s'executa al core 0 que correspon al 
//            consumidor. Aquesta tsca és l'encarregada d'empaquetar les dades i enviar-les per un websocket.

void onMessageCallback(WebsocketsMessage message) {
    Serial.print("Got Message: ");
    Serial.println(message.data());
}

void onEventsCallback(WebsocketsEvent event, String data) {
    if(event == WebsocketsEvent::ConnectionOpened) {
        Serial.println("Connexió websocket " + String(websockets_server) + " establerta");
    } else if(event == WebsocketsEvent::ConnectionClosed) {
        Serial.println("*** CONNEXIÓ TANCADA ***");
    } else if(event == WebsocketsEvent::GotPing) {
        Serial.println("Got a Ping!");
    } else if(event == WebsocketsEvent::GotPong) {
        Serial.println("Got a Pong!");
    }
}

void consumerHandler(void *param) {
  Serial.println("[Core " + String(xPortGetCoreID()) + "] consumer running");
  while (true) {
    // Sleep until the ISR gives us something to do, or for 3 second
    uint32_t tcount = ulTaskNotifyTake(pdFALSE, portMAX_DELAY);  

    //Enviar pel websocket...
    client.send(buffers[(current_buffer+1)%NUM_BUFFERS]);
  }
}

void IRAM_ATTR onTimer() {
  portENTER_CRITICAL_ISR(&timerMux);

  int senyal = analogRead(ANALOG_READ_PIN);

  if(num_elements == 0) buffers[current_buffer] = String(senyal);
  else buffers[current_buffer] += ";" + String(senyal);
  //buffers[current_buffer] += ";" + String(senyal);
  num_elements++;
  
  if(num_elements>=NUM_SAMPLES_PACKET) {
    current_buffer = (current_buffer+1) % NUM_BUFFERS;
    num_elements=0;
    buffers[current_buffer] = "";
    
    // Notifiquem al consumidor que té un paquet a punt...
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    vTaskNotifyGiveFromISR(consumidorTask, &xHigherPriorityTaskWoken);
    
    if (xHigherPriorityTaskWoken) portYIELD_FROM_ISR();
  }
  portEXIT_CRITICAL_ISR(&timerMux);
}

void setup() {
  Serial.begin(115200);
  Serial.println("");
  Serial.println("   ____  ____  _____  ____    _    _  ____  ____  ____  ");
  Serial.println("  (_  _)(_  _)(  _  )(_  _)  ( \\/\\/ )(_  _)( ___)(_  _) ");
  Serial.println("   _)(_  _)(_  )(_)(   )(     )    (  _)(_  )__)  _)(_  ");
  Serial.println("  (____)(____)(_____) (__)   (__/\\__)(____)(__)  (____) ");
  Serial.println("");
  Serial.println("Pràctica Comunicacions 2019. Professors: Carles Pous i Xavier Pinsach.");
  Serial.println("");

  for(int i=0;i < NUM_BUFFERS; i++) 
    buffers[i]="";

  // Connect to wifi
  Serial.println("Connectant a la Wifi amb SSID " + String(ssid));
  WiFi.begin(ssid, password);

  if (WiFi.waitForConnectResult() == WL_CONNECTED) {  
    Serial.println("Connexió Wifi amb SSID " + String(ssid) + " establerta amb IP " + WiFi.localIP().toString());

    // Setup Callbacks
    client.onEvent(onEventsCallback);
    
    // Connect to server
    Serial. println("");
    Serial.println("Connectant amb el websocket server " + String(websockets_server));
    client.connect(websockets_server);

    Serial.println("");
    Serial.println("[Core " + String(xPortGetCoreID()) + "] producer running");
    //Crea la tasca del consumidor enllaçada al core 0
    xTaskCreatePinnedToCore(consumerHandler, "Handler Task", 8192, NULL, 1, &consumidorTask, 0);
  
   
    timerproducer = timerBegin(3, 80, true); // 80 MHz / 80 = 1 MHz hardware clock for easy figuring
    timerAttachInterrupt(timerproducer, &onTimer, true); // Attaches the handler function to the timer 
    timerAlarmWrite(timerproducer, 83, true); // Interrupts when counter == 45, i.e. 22.222 times a second
    timerAlarmEnable(timerproducer);  
  } else {
    Serial.println("WiFi Failed");
  }
}

void loop() {}
