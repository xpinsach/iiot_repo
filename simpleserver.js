const WebSocket = require('ws');
var ip = require("ip");

var num_data = 0;
var start = new Date()
var simulateTime = 1000;
                                                                                 
console.log('');
console.log('   ____  ____  _____  ____    _    _  ____  ____  ____  ');
console.log('  (_  _)(_  _)(  _  )(_  _)  ( \\/\\/ )(_  _)( ___)(_  _) ');
console.log('   _)(_  _)(_  )(_)(   )(     )    (  _)(_  )__)  _)(_  ');
console.log('  (____)(____)(_____) (__)   (__/\\__)(____)(__)  (____) ');
console.log('');
console.log("Pràctica Comunicacions 2019. Professors: Carles Pous i Xavier Pinsach.");
console.log('');
process.stdout.write("Inicialitzant servidor websocket " + ip.address() + ":8080")
const wss = new WebSocket.Server({ port: 8080 });
console.log("...fet!");

process.stdout.write("Inicialitzant servidor websocket " + ip.address() + ":8081")
const wss2 = new WebSocket.Server({ port: 8081 });
console.log("...fet!");

wss.on('connection', function connection(ws) {
  console.log('Nova connexió de l\'adreça remota ' + ws._socket.remoteAddress + ':' + ws._socket.remotePort);

  setInterval(function(argument) {
    // execution time simulated with setTimeout function
    var end = new Date() - start
    xstart = start;
    start = new Date();
    let num = num_data;
    num_data=0;
    process.stdout.write("Elapsed time: " + end + " ms   Dades: " + num + "   Velocitat: " + Math.round(num/(end/1000)) + " dades/s\r");
  }, simulateTime)

  ws.on('message', function incoming(message) {
    //console.log('received: %s', message.split(';').length);
    num_data += message.split(';').length;
    wss2.clients.forEach(function each(client) {
      if (client.readyState === WebSocket.OPEN) {
        client.send(message);
      }
    });
  });
});

wss2.on('connection', function connection(ws) {
  console.log('Nova connexió de l\'adreça remota ' + ws._socket.remoteAddress + ':' + ws._socket.remotePort);
});