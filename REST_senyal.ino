#define ANALOG_READ_PIN 34 //Pin d'on llegim la senyal analògica

#include <WiFi.h>
#include <HTTPClient.h>

const char* ssid = "SSID";
const char* password = "PASSWORD";

void setup() {
  Serial.begin(115200);
  Serial.println("Pràctica Comunicacions 2019. Professors: Carles Pous i Xavier Pinsach.");
  Serial.println("Adquisició senyal sinosuïdal.");
  Serial.println("");

  WiFi.begin(ssid, password); 
 
  while (WiFi.status() != WL_CONNECTED) { //Check for the connection
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }
 
  Serial.println("Connected to the WiFi network");

}

void loop() {
  if(WiFi.status()== WL_CONNECTED){   //Check WiFi connection status
    //Llegim la senyal
    long ini = micros();
    int senyal = analogRead(ANALOG_READ_PIN);

    HTTPClient http;   

    http.begin("http://192.168.43.162:8001/testpost");  //Specify destination for HTTP request
    http.addHeader("Content-Type", "text/plain");             //Specify content-type header
    
    int httpResponseCode = http.POST(String(senyal));   //Send the actual POST request
    
    if(httpResponseCode>0){
    } else {
      Serial.print("Error on sending POST: ");
      Serial.println(httpResponseCode);
    }
    http.end();
    //Fem un delay
    long interval=micros()-ini;
    Serial.println(interval);
  }
  else{
    Serial.println("Error in WiFi connection");   
  }
}
